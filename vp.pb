; **********************************************************************
; *                                                                    *
; *                  VirtualParadise for PURE BASIC                    *
; *                             Rev A                                  *
; *                       Adapted by Neophile                          *
; *                           (c) 2018                                 *
; *                                                                    *
; *                         Enjoy it ;-)                               *
; *                                                                    *
; *                                                                    *
; *                                                                    *
; **********************************************************************

; Enumerations

#VPSDK_VERSION=4

;vp_event_t

Enumeration
  #VP_EVENT_CHAT
  #VP_EVENT_AVATAR_ADD
  #VP_EVENT_AVATAR_CHANGE
  #VP_EVENT_AVATAR_DELETE
  #VP_EVENT_OBJECT
  #VP_EVENT_OBJECT_CHANGE
  #VP_EVENT_OBJECT_DELETE
  #VP_EVENT_OBJECT_CLICK
  #VP_EVENT_WORLD_LIST
  #VP_EVENT_WORLD_SETTING
  #VP_EVENT_WORLD_SETTINGS_CHANGED
  #VP_EVENT_FRIEND
  #VP_EVENT_WORLD_DISCONNECT
  #VP_EVENT_UNIVERSE_DISCONNECT
  #VP_EVENT_USER_ATTRIBUTES
  #VP_EVENT_CELL_END
  #VP_EVENT_TERRAIN_NODE
  #VP_EVENT_AVATAR_CLICK
  #VP_EVENT_TELEPORT
  #VP_EVENT_URL
  #VP_EVENT_OBJECT_BUMP_BEGIN
  #VP_EVENT_OBJECT_BUMP_END
  #VP_EVENT_TERRAIN_NODE_CHANGED
  #VP_EVENT_JOIN
  #VP_HIGHEST_EVENT
EndEnumeration

;vp_callback_t

Enumeration
  #VP_CALLBACK_OBJECT_ADD
	#VP_CALLBACK_OBJECT_CHANGE
	#VP_CALLBACK_OBJECT_DELETE
  #VP_CALLBACK_GET_FRIENDS
  #VP_CALLBACK_FRIEND_ADD
  #VP_CALLBACK_FRIEND_DELETE
  #VP_CALLBACK_TERRAIN_QUERY
  #VP_CALLBACK_TERRAIN_NODE_SET
  #VP_CALLBACK_OBJECT_GET
  #VP_CALLBACK_OBJECT_LOAD
  #VP_CALLBACK_LOGIN
  #VP_CALLBACK_ENTER
	#VP_CALLBACK_JOIN
	#VP_CALLBACK_CONNECT_UNIVERSE
  #VP_CALLBACK_WORLD_PERMISSION_USER_SET
  #VP_CALLBACK_WORLD_PERMISSION_SESSION_SET
  #VP_CALLBACK_WORLD_SETTING_SET
  #VP_HIGHEST_CALLBACK
EndEnumeration

;vp_int_attribute_t
Enumeration
  #VP_AVATAR_SESSION
	#VP_AVATAR_TYPE
	#VP_MY_TYPE	
	#VP_OBJECT_ID
	#VP_OBJECT_TYPE
	#VP_OBJECT_TIME
	#VP_OBJECT_USER_ID
	#VP_WORLD_STATE
	#VP_WORLD_USERS
	#VP_REFERENCE_NUMBER
	#VP_CALLBACK
	#VP_USER_ID
  #VP_USER_REGISTRATION_TIME
	#VP_USER_ONLINE_TIME
	#VP_USER_LAST_LOGIN
	#VP_FRIEND_ID
	#VP_FRIEND_USER_ID
	#VP_FRIEND_ONLINE
	#VP_MY_USER_ID
	#VP_PROXY_TYPE
	#VP_PROXY_PORT
	#VP_CELL_X
	#VP_CELL_Z
  #VP_TERRAIN_TILE_X
  #VP_TERRAIN_TILE_Z
  #VP_TERRAIN_NODE_X
  #VP_TERRAIN_NODE_Z
  #VP_TERRAIN_NODE_REVISION
  #VP_CLICKED_SESSION
  #VP_CHAT_TYPE
  #VP_CHAT_COLOR_RED
  #VP_CHAT_COLOR_GREEN
  #VP_CHAT_COLOR_BLUE
  #VP_CHAT_EFFECTS
  #VP_DISCONNECT_ERROR_CODE
  #VP_URL_TARGET
  #VP_CURRENT_EVENT
  #VP_CURRENT_CALLBACK
  #VP_CELL_REVISION
  #VP_CELL_STATUS
	#VP_JOIN_ID
  #VP_HIGHEST_INT
EndEnumeration

;vp_float_attribute_t
Enumeration
  #VP_AVATAR_X
  #VP_AVATAR_Y
  #VP_AVATAR_Z
  #VP_AVATAR_YAW
  #VP_AVATAR_PITCH
  #VP_MY_X
  #VP_MY_Y
  #VP_MY_Z
  #VP_MY_YAW
  #VP_MY_PITCH
  #VP_OBJECT_X
  #VP_OBJECT_Y
  #VP_OBJECT_Z
	#VP_OBJECT_ROTATION_X
	#VP_OBJECT_ROTATION_Y
	#VP_OBJECT_ROTATION_Z
	#VP_OBJECT_YAW = #VP_OBJECT_ROTATION_X
	#VP_OBJECT_PITCH = #VP_OBJECT_ROTATION_Y
	#VP_OBJECT_ROLL = #VP_OBJECT_ROTATION_Z
	#VP_OBJECT_ROTATION_ANGLE
  #VP_TELEPORT_X
  #VP_TELEPORT_Y
  #VP_TELEPORT_Z
  #VP_TELEPORT_YAW
  #VP_TELEPORT_PITCH  
  #VP_CLICK_HIT_X
  #VP_CLICK_HIT_Y
  #VP_CLICK_HIT_Z
	#VP_JOIN_X
	#VP_JOIN_Y
	#VP_JOIN_Z
	#VP_JOIN_YAW
	#VP_JOIN_PITCH
	#VP_HIGHEST_FLOAT
EndEnumeration

;vp_string_attribute_t
Enumeration
	#VP_AVATAR_NAME
	#VP_CHAT_MESSAGE
	#VP_OBJECT_MODEL
	#VP_OBJECT_ACTION
	#VP_OBJECT_DESCRIPTION
	#VP_WORLD_NAME
	#VP_USER_NAME
	#VP_USER_EMAIL
	#VP_WORLD_SETTING_KEY
	#VP_WORLD_SETTING_VALUE
	#VP_FRIEND_NAME
	#VP_PROXY_HOST
  #VP_TELEPORT_WORLD
  #VP_URL
	#VP_JOIN_WORLD
	#VP_JOIN_NAME
  #VP_START_WORLD
	#VP_HIGHEST_STRING
EndEnumeration

;vp_data_attribute_t
Enumeration
  #VP_OBJECT_DATA
  #VP_TERRAIN_NODE_DATA
	#VP_HIGHEST_DATA
EndEnumeration

;vp_proxy_type_t
Enumeration
	#VP_PROXY_TYPE_NONE
	#VP_PROXY_TYPE_SOCKS4A
EndEnumeration

;vp_url_target_t
Enumeration
  #VP_URL_TARGET_BROWSER
  #VP_URL_TARGET_OVERLAY
EndEnumeration

;vp_cell_status_t
Enumeration
  #VP_CELL_STATUS_MODIFIED
  #VP_CELL_STATUS_NOT_MODIFIED
  #VP_CELL_STATUS_ERROR
EndEnumeration

;VPReturnCode
Enumeration
  #VP_RC_SUCCESS
	#VP_RC_VERSION_MISMATCH
	#VP_RC_NOT_INITIALIZED
  #VP_RC_ALREADY_INITIALIZED
	#VP_RC_STRING_TOO_LONG
	#VP_RC_INVALID_LOGIN
	#VP_RC_WORLD_NOT_FOUND
	#VP_RC_WORLD_LOGIN_ERROR
	#VP_RC_NOT_IN_WORLD
	#VP_RC_CONNECTION_ERROR
	#VP_RC_NO_INSTANCE
	#VP_RC_NOT_IMPLEMENTED
	#VP_RC_NO_SUCH_ATTRIBUTE
	#VP_RC_NOT_ALLOWED
	#VP_RC_DATABASE_ERROR
	#VP_RC_NO_SUCH_USER
	#VP_RC_TIMEOUT
  #VP_RC_NOT_IN_UNIVERSE
  #VP_RC_INVALID_ARGUMENTS
  #VP_RC_OBJECT_NOT_FOUND
  #VP_RC_UNKNOWN_ERROR
  #VP_RC_RECURSIVE_WAIT
  #VP_RC_JOIN_DECLINED
  #VP_RC_SECURE_CONNECTION_REQUIRED
  #VP_RC_HANDSHAKE_FAILED
  #VP_RC_VERIFICATION_FAILED
  #VP_RC_NO_SUCH_SESSION
  #VP_RC_NOT_SUPPORTED
EndEnumeration

Structure vp_terrain_cell_t
  height.f
  attributes.u
EndStructure

; D�claration des fonctions de la SDK
Prototype.i Proto_vp_init(Version.i = #VPSDK_VERSION)
Prototype.i Proto_vp_create(*NetConfig)
Prototype.i Proto_vp_int(Instance.i, Attribute.i)
Prototype.f Proto_vp_float(Instance.i, Attribute.i)
Prototype.i Proto_vp_float_set(Instance.i, Attribute.l,Value.f)
Prototype.d Proto_vp_double(Instance.i, Attribute.i)
;Prototype.s Proto_vp_string(Instance.i, Attribute.i)
Prototype.i Proto_vp_string_set(Instance.i, Attribute.i,Value.p-unicode)
Prototype.i Proto_vp_event_set(Instance.i,Event_Name.i, Event.i )
Prototype.i Proto_vp_connect_universe(Instance.i,Host.p-utf8, Port.i )
Prototype.i Proto_vp_login(Instance.i,UserName.p-utf8, Password.p-utf8, BotName.p-utf8 )
Prototype.i Proto_vp_enter(Instance.i,WorldName.p-utf8)
Prototype.i Proto_vp_state_change(Instance.i)
Prototype.i Proto_vp_wait(Instance.i, Milliseconds.i)
Prototype.i Proto_vp_leave(Instance.i)
Prototype.i Proto_vp_destroy(Instance.i)
Prototype.i Proto_vp_say(Instance.i, Message.p-utf8)

Global vp_init.Proto_vp_init
Global vp_create.Proto_vp_create
Global vp_int.Proto_vp_int
Global vp_float.Proto_vp_float
Global vp_float_set.Proto_vp_float_set
Global vp_double.Proto_vp_double
Global vp_string.i
Global vp_string_set.Proto_vp_string_set
Global vp_event_set.Proto_vp_event_set
Global vp_connect_universe.Proto_vp_connect_universe
Global vp_login.Proto_vp_login
Global vp_enter.Proto_vp_enter
Global vp_state_change.Proto_vp_state_change
Global vp_wait.Proto_vp_wait
Global vp_leave.Proto_vp_leave
Global vp_destroy.Proto_vp_destroy
Global vp_say.Proto_vp_say

Procedure.b VPSDK_Init()
  CompilerIf #PB_Compiler_OS = #PB_OS_Linux
    DLL = OpenLibrary (0,"libvpsdk.so")
  CompilerElseIf #PB_Compiler_OS = #PB_OS_MacOS
    DLL = OpenLibrary (0,"libvpsdk.dylib")
  CompilerElseIf #PB_Compiler_OS = #PB_OS_Windows
    DLL = OpenLibrary (0,"vpsdk.dll")
  CompilerElse
    ProcedureReturn 0
  CompilerEndIf
  If DLL
    vp_init = GetFunction (0, "vp_init")
    vp_create = GetFunction (0, "vp_create")
    vp_int = GetFunction (0, "vp_int")
    vp_float = GetFunction (0, "vp_float")
    vp_float_set = GetFunction (0, "vp_float_set")
    vp_double = GetFunction (0, "vp_double")
    vp_string = GetFunction (0, "vp_string")
    vp_string_set = GetFunction (0, "vp_string_set")
    vp_event_set = GetFunction (0, "vp_event_set")
    vp_connect_universe = GetFunction (0, "vp_connect_universe")
    vp_login = GetFunction (0, "vp_login")
    vp_enter = GetFunction (0, "vp_enter")
    vp_state_change = GetFunction (0, "vp_state_change")
    vp_wait = GetFunction (0, "vp_wait")
    vp_leave = GetFunction (0, "vp_leave")
    vp_destroy = GetFunction (0, "vp_destroy")
    vp_say = GetFunction (0, "vp_say")
    ProcedureReturn 1
  EndIf
  ProcedureReturn 0
EndProcedure

Procedure.s VP_string(a.i, b.i); R�cup�re le texte de l'attribut donn�
  ProcedureReturn PeekS(CallCFunctionFast(vp_string,a,b),-1,#PB_UTF8)
EndProcedure


; End-Funktion
Procedure VPSDK_End()
  CloseLibrary (0)
EndProcedure
; IDE Options = PureBasic 5.70 LTS (MacOS X - x64)
; CursorPosition = 284
; FirstLine = 253
; Folding = -
; Executable = greeter
; CurrentDirectory = /home/gianni/purebasic/examples/sources/
; CompileSourceDirectory