#Virtual Paradise SDK Wrapper for PureBasic language

[Virtual Paradise](https://www.virtualparadise.org/) is an alternative to virtual world technologies like Second Life or ActiveWorlds.

To run this sample , you'll need to get the [VP SDK](http://dev.virtualparadise.org/downloads.php)  for your platform

You'll need also a valid account to connect you bot on the VP universe.

PureBasic is a payware programming language which aims to be simple to write and deploy.

Purebasic toolbox contains a lot of usefull features to code games , utils, daemons, lib applications (Ogre3D Engine, Network, threads, 2D , Window Widgets, etc)

Pure Basix is optimized in asm to compile very fast and small executable on you platform.

You can try or buy it on the PureBasic [website](https://www.purebasic.com/french/)